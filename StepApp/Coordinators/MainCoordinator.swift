//
//  Coordinator.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 23.06.2022.
//

import Foundation
import UIKit

final class MainCoordinator: CoordinatorsProtocol {
    var navigation: UINavigationController
    var parentCoordinator: MainCoordinator?
    var children: [MainCoordinator] = []
    
    // MARK: - Init
    init(_ navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    private func show(viewController: UIViewController, by showingType: ShowingType) {
        switch showingType {
        case .push:
            navigation.pushViewController(viewController, animated: true)
            
        case .present:
            navigation.present(viewController, animated: true, completion: nil)
        }
    }
    
    func start() {
        guard let startViewController = UIStoryboard(name: .main)
            .instantiateViewController(withIdentifier: "StartViewController") as? StartViewController else {
            return
        }
        startViewController.viewModel = StartViewModel()
        startViewController.viewModel.coordinator = self
        show(viewController: startViewController, by: .push)
    }
    
    // MARK: - Coordinate to
    func showActivityVC(viewModel: ActivityViewModel, showingType: ShowingType) {
        let activityViewController = UIStoryboard(name: .main).viewController(type: ActivityViewController.self)
        activityViewController.viewModel = viewModel
        activityViewController.viewModel.coordinator = self
        show(viewController: activityViewController, by: .push)
    }
    
    func popToRoot() {
        navigation.popToRootViewController(animated: true)
    }
}
