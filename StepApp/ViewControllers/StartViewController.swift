//
//  StartViewController.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 21.06.2022.
//

import UIKit

final class StartViewController: UIViewController {
    // MARK: - Private properties
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var startTraningImageView: UIImageView!
    
    // MARK: - ViewModel
    var viewModel: StartViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        gesture()
    }
    
    // MARK: - Private methods
    private func configureUI() {
        startTraningImageView.image = UIImage(named: "running")
        containerView.layer.cornerRadius = containerView.bounds.height / 2
        startTraningImageView.layer.cornerRadius = containerView.bounds.height / 2
        containerView.setShadow(colorShema: .red)
    }
    
    private func gesture() {
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.startTraining))
        containerView.addGestureRecognizer(gesture)
    }
    
    @objc private func startTraining() {
        viewModel.coordinator.showActivityVC(viewModel: ActivityViewModel(), showingType: .push)
    }
}
