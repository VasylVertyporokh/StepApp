//
//  ActivityViewController.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 21.06.2022.
//

import Foundation
import UIKit

final class ActivityViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var timeLabel: UILabel!
    @IBOutlet private var distanceLabel: UILabel!
    @IBOutlet private var stepLabel: UILabel!
    @IBOutlet private var tempLabel: UILabel!
    @IBOutlet private var distanceKmLabel: UILabel!
    @IBOutlet private var numberOfStepsLabel: UILabel!
    @IBOutlet private var averageTempLabel: UILabel!
    
    @IBOutlet private var distanceImageView: UIImageView!
    @IBOutlet private var stepsImageView: UIImageView!
    @IBOutlet private var tempImageView: UIImageView!
    
    @IBOutlet private var primaryButton: UIButton!
    
    // MARK: - ViewModel
    var viewModel: ActivityViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        updateActivityDetails()
    }
    
    // MARK: - ConfigureUI
    private func configureUI() {
        title = "Activity"
        navigationController?.navigationBar.tintColor = .red
        
        distanceImageView.image = UIImage(named: "walk")
        stepsImageView.image = UIImage(named: "step")
        tempImageView.image = UIImage(named: "speedometer")
        
        infoLabel.text = "Time of activity"
        distanceLabel.text = "Distance"
        stepLabel.text = "Steps"
        tempLabel.text = "Avg.tempo"
        
        timeLabel.text = viewModel.userActivityModel.totalTime
        distanceKmLabel.text = viewModel.userActivityModel.distance
        numberOfStepsLabel.text = viewModel.userActivityModel.numberOfSteps
        averageTempLabel.text = viewModel.userActivityModel.averageActivity
        
        primaryButton.setImage(UIImage(named: "playIcon"), for: .normal)
        primaryButton.tintColor = .red
        primaryButton.setTitle("Start", for: .normal)
        primaryButton.setTitleColor(.red, for: .normal)
        primaryButton.layer.cornerRadius = primaryButton.bounds.height / 2
        primaryButton.layer.borderColor = UIColor.red.cgColor
        primaryButton.layer.borderWidth = 1.5
    }
    
    private func updateActivityDetails() {
        viewModel.dataResult = { [weak self] result in
            guard let self = self else {
                return
            }
            self.timeLabel.text = result.totalTime
            self.distanceKmLabel.text = result.distance
            self.numberOfStepsLabel.text = result.numberOfSteps
            self.averageTempLabel.text = result.averageActivity
        }
    }
    
    // MARK: - Actions 
    @IBAction private func primaryButtonAction(_ sender: UIButton) {
        viewModel.handleTraining()
        primaryButton.setTitle(viewModel.isStartedTraining ? "Stop" : "Start", for: .normal)
        primaryButton.setImage(UIImage(named: viewModel.isStartedTraining ? "stop" : "playIcon"), for: .normal)
    }
}
