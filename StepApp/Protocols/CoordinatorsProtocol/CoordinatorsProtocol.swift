//
//  CoordinatorsProtocol.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 05.07.2022.
//

import Foundation
import UIKit

// MARK: - Showing Types
enum ShowingType {
    case push
    case present
}

protocol CoordinatorsProtocol {
    var parentCoordinator: MainCoordinator? { get set }
    var children: [MainCoordinator] { get set }
    var navigation : UINavigationController { get set }
    func start()
}
 
