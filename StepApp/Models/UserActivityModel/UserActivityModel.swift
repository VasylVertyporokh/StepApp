//
//  UserActivityModel.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 23.06.2022.
//

import Foundation

struct UserActivityModel {
    var totalTime: String
    var numberOfSteps: String
    var averageActivity: String
    var distance: String
}
