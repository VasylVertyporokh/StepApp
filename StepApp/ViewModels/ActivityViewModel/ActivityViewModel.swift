//
//  ActivityViewModel.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 22.06.2022.
//

import Foundation

final class ActivityViewModel {
    // MARK: - Private properties
    private let pedometer = Pedometer()
    private var timer = Timer()
    private var count = 0
    private(set) var isStartedTraining = false
    private(set) var userActivityModel = UserActivityModel(
        totalTime: "00:00:00",
        numberOfSteps: "0",
        averageActivity: "0.00",
        distance: "0.00 km"
    )
    
    // MARK: - Public properties
    var dataResult: ((UserActivityModel)-> Void)?
    var coordinator: MainCoordinator!
    
    // MARK: - Public methods
    func startTraining() {
        pedometer.startUpdateActivity { [weak self] pedometerData in
            guard let self = self,
                  let numberOfSteps = pedometerData?.numberOfSteps.intValue,
                  let averageActive = pedometerData?.currentPace,
                  let distance = pedometerData?.distance else {
                return
            }
            self.userActivityModel.numberOfSteps = "\(numberOfSteps)"
            self.userActivityModel.averageActivity = String(format: "%.2f", averageActive.doubleValue)
            self.userActivityModel.distance = String(format: "%.2f km", distance.doubleValue / 1000)
        }
        isStartedTraining = true
        timer = .scheduledTimer(timeInterval: 1,
                                target: self,
                                selector: #selector(startCount),
                                userInfo: nil,
                                repeats: true)
    }
    
    func handleTraining() {
        isStartedTraining ? stopTraining() : startTraining()
    }
    
    // MARK: - Private methods
    private func stopTraining() {
        pedometer.stopUpdate()
        resetModel()
        timer.invalidate()
        isStartedTraining = false
    }
    
    private func resetModel() {
        userActivityModel.totalTime = "00:00:00"
        userActivityModel.numberOfSteps = "0"
        userActivityModel.averageActivity = "0.00"
        userActivityModel.distance = "0.00 km"
        count = 0
    }
    
    @objc private func startCount() {
        count += 1
        userActivityModel.totalTime = count.makeTimeString
        guard let dataResult = dataResult else {
            return
        }
        dataResult(userActivityModel)
    }
}
