//
//  UIStoryboard.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 30.06.2022.
//

import Foundation
import UIKit

// MARK: - StoryboardsNames
enum StoryboardName: String {
    case main
    
    var id: String { return rawValue.capitalized }
}

extension UIStoryboard {
    convenience init(name: StoryboardName) {
        self.init(name: name.id, bundle: nil)
    }
    
    func viewController<Type: UIViewController>(type: Type.Type) -> Type {
        return controller(id: String(describing: type), type: type)
    }
    
    private func controller<Type>(id: String, type: Type.Type) -> Type {
        return instantiateViewController(withIdentifier: id) as! Type
    }
    
}
