//
//  Int+Exstansion.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 04.07.2022.
//

import Foundation

extension Int {
    // make time string from Int value like 60 seconds equal 00:01:00
    var makeTimeString: String {
        let hours = self / 3600
        let minutes = (self % 3600) / 60
        let seconds = (self % 3600) % 60
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
}
