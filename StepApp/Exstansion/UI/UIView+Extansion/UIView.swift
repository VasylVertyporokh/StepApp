//
//  UIView.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 21.06.2022.
//

import Foundation
import UIKit

enum MainShadowColor {
    case red
}

extension UIView {
    func setShadow(colorShema: MainShadowColor) {
        switch colorShema {
        case .red:
            self.layer.shadowRadius = 10
            self.layer.shadowOpacity = 1
            self.layer.shadowColor = UIColor.red.cgColor
            self.layer.shadowOffset = .zero
        }
    }
}
