//
//  Pedometer.swift
//  StepApp
//
//  Created by Vasil Vertiporokh on 04.07.2022.
//

import Foundation
import CoreMotion

final class Pedometer {
    // MARK: - Private properties
    private let pedometer = CMPedometer()
    private var pedometerData: CMPedometerData?
    
    // MARK: - Public methods
    func startUpdateActivity(clousure: @escaping (CMPedometerData?) -> Void) {
        pedometer.startUpdates(from: Date()) { [weak self] pedometerData, _ in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.pedometerData = pedometerData
                clousure(self.pedometerData)
            }
        }
    }
    
    func stopUpdate() {
        pedometer.stopUpdates()
        pedometerData = nil
    }
}
